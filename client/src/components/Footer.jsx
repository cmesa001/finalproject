import React from "react";

function Footer() {//function that returns the footer HTML component 
  return (
    <div className="footer">
      <footer class="py-1 bg-dark fixed-bottom">
        <div class="container">
          <p class="m-0 text-center text-white">
            Copyright &copy; Catalin Mesaros 2021
          </p>
        </div>
      </footer>
    </div>
  );
}

export default Footer;