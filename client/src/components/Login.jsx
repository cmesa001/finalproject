//imports 
import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from '../Utils/Common';
import { withRouter } from 'react-router-dom';

function Login(props) {
  const [loading, setLoading] = useState(false);//set the loading variables
  const username = useFormInput('');// username form
  const password = useFormInput('');//password form
  const [error, setError] = useState(null);//set error state

  // handle button click of login form
  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios.post('http://localhost:4000/users/signin',//post information from form to the backend 
      {
        username: username.value, password: password.value
      })
      .then(response => {//deal with the response 
        setLoading(false);
        if (response.message)
          console.log(response.message);
        else {
          setUserSession(response.data.token, response.data.user);//set user session
          props.history.push('/dashboard');//redirect to dashboard
        }
      })
      .catch(error => {//hande error 
        setLoading(false);
        console.log(error.response);
      });
  }

  return ( // HTML component that prints the login form and button
    <div>
      Login<br /><br />
      <div>
        Username<br />
        <input type="text" {...username} autoComplete="new-password" />
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" {...password} autoComplete="new-password" />
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br /> 
    </div>
  );
}

const useFormInput = initialValue => {//handle changes in forms
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default withRouter(Login);