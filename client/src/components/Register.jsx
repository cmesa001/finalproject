//imports
import React, { useState } from 'react';
import axios from 'axios';
import { setUserSession } from '../Utils/Common';
import { withRouter } from 'react-router-dom';

function Register(props) {
  const [loading, setLoading] = useState(false);//set state for loading 
  const name = useFormInput('');//name form
  const username = useFormInput('');//username form
  const email = useFormInput('');//email form
  const password = useFormInput('');//password form
  const [error, setError] = useState(null);//error state 

  const handleRegister = () => {//handle register function
    setError(null);
    setLoading(true);
    console.log(name.value + username.value + email.value + password.value);
    axios.post('http://localhost:4000/users/registerin',//post form values to backend
      {
        name: name.value, username: username.value,
        email: email.value, password: password.value
      }).then(response => {//deal with response
        setLoading(false);
        setUserSession(response.data.token, response.data.user);
        props.history.push('/dashboard');
      }).catch(error => {//deal with errors
        setLoading(false);
        console.log(error.response);
      });
  }
  return (//return HTML component with all the forms necessary and the register button
    <div>
      Register <br /><br />
      <div>
        Full Name<br />
        <input type="text" {...name} autoComplete="new-password" />
      </div>
      <div style={{ marginTop: 10 }}>
        Username<br />
        <input type="text" {...username} autoComplete="new-password" />
      </div>
      <div style={{ marginTop: 10 }}>
        Email<br />
        <input type="text" {...email} autoComplete="new-password" />
      </div>
      <div style={{ marginTop: 10 }}>
        Password<br />
        <input type="password" {...password} autoComplete="new-password" />
      </div>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <input type="button" value={loading ? 'Loading...' : 'Register'} onClick={handleRegister} disabled={loading} /><br />
    </div>
  );
}
const useFormInput = initialValue => {// function that deals with changes in user form
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}
export default withRouter(Register);