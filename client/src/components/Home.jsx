import React from "react";

function Home() {//function that returns the homepage HTML component
  return (
    <div>
      <br /><br /><br />
      <h1>Welcome to the Movie Recommendation Home Page!</h1>
    </div>
  );
}

export default Home;