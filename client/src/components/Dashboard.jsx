//importing necessary libraries and modules from other files
import React, {useState,useEffect} from 'react';
import { getUser, removeUserSession } from '../Utils/Common';
import {withRouter} from 'react-router-dom';
import axios from 'axios';


const items = ['Adventure','Animation','Children','Comedy','Fantasy',
'Romance','Drama','Action','Crime','Thriller','Horror','Mystery','Sci-Fi',
'Documentary','War','Musical','Western','Film-Noir', '(no genres listed)'];// list of genres
let rep= [];// list of recommendation
let tmp={};// temporary object

const Dashboard = (props)=>{
  // function dealing with changes for the user id field
  const useFormInput = initialValue => { 
    const [value, setValue] = useState(initialValue);
  
    const handleChange = e => {
      setValue(e.target.value);
    }
    return {
      value,
      onChange: handleChange
    }
  }

  const List = (props) => { // list function which return HTML component where the prediction list is going to be displayed
    const { repos } = props;// asigning the list to constant repos
    if (!repos || repos.length === 0) return <p>No movies available :(, sorry</p>;//if the list is empty return this element
    return (//if the list is not empty wwe return a list with the movie along with their information 
      <ul>
        <h2 className='list-head'>We think that you might like these movies:</h2>
        {repos.map( (repo) => {
          return (
            <li  className='list' list-style-type="none">
              <span className='repo-text'>{repo.title} </span>
              <br />
              <span className='repo-poster'>
              <img 
              src={repo.poster}
              alt={repo.title}
              />
              </span>
              <br />
              <span className='repo-link'>IMDB page :
              <a target="_blank" href={repo.link}>{repo.link}</a>
              </span> 
              <br />
              <span className='repo-score'>IMDB score :{repo.score}</span>
              <br />
              <hr class="solid"></hr>
            </li>
          );
        })}
      </ul>
    );
  };
  const withListLoading = (Component) => { // function that displays messege while awaiting for the prediction list 
    return function WihLoadingComponent({ isLoading, ...props }) {
      if (!isLoading) return <Component {...props} />;
      return (
        <p style={{ textAlign: 'center', fontSize: '30px' }}>
          Hold on, fetching data may take some time :)
        </p>
      );
    };
  }
  const userId = useFormInput('');//user id input 
  const [checkedItems, setCheckedItems] = useState({});// variables for genres selected
  const user = getUser();// constant where we store user details 
  const ListLoading = withListLoading(List);
  const [appState, setAppState] = useState({//constant for dealing with the app state
    loading: false,
    repos: null,
  });

  useEffect(() => {//function to change the app state
    setAppState({ loading: true });
  },[setAppState]);
  const tagSet = new Set();// set of genres
  

  const Checkbox = ({ type = "checkbox", checked = false, name, onChange }) => {//function that returns a checklist element 
    return (
      <input type={type} checked={checked} name={name}  onChange={onChange} />
    );
  };
  
    const handleChange = event => {//function that renders the checkboxes 
      setCheckedItems({
        ...checkedItems,
        [event.target.name]: event.target.checked
      });
   
      console.log("checkedItems: ", checkedItems);
      console.log()
    };
    const checkboxes = [];
    for (var i=0;i<items.length;i++){
      var box =
        {
          name: items[i],
          key: i,
          label: "checkBox" + i
        }
      checkboxes.push(box);
    }
  // handle click event of logout button
  const handlePrediction = async() =>{//async function dealing with predictions 
    rep=[];
    setAppState({ loading: true, repos:rep });//change app state
    for(let i=0;i<20;i++)//we add the checked boxes to the set
        if (checkedItems[items[i]]===true)
          {
            tagSet.add(items[i]);
          }
          
    const tagSet1= Array.from(tagSet);      
    console.log(tagSet1);
    console.log("start")
    axios.post('http://localhost:4000/prediction',{//we post the variables to the back-end 
          tagSet:tagSet1,
          userId:userId
    }).then(resp => {//dealing with the response
      const response = resp.data;
      let imdb = response.imdb;
      response.list.forEach(element => {// create list of recommendation
        tmp = {
          title:element[0],
          link:imdb[element[1]].Link,
          score:imdb[element[1]].Score,
          poster:imdb[element[1]].Poster,
        }
        rep.push(tmp);
      });
      setAppState({ loading: true });
      setAppState({ loading: false, repos: rep });//change the app state 
      console.log("end");
    })
    
    
  }
  const handleLogout = () => {    //function that handles the logout button 
    removeUserSession();
    props.history.push('/login');
  }
  
  return (//we return the main HTML component 
    <div>
      Welcome {user.name}!
      <br /><br />
      <lable>Select at least one genre :  </lable> <br/>
          {//we display the checkboxes 
              checkboxes.map(item => (
                  <label key={item.key}>
                      {item.name}
                      <Checkbox name={item.name} checked={checkedItems[item.name]} onChange={handleChange} />
                  </label>
              ))
          }
          <br></br>
      <div>
        User Id (1-25000)<br />
        {/* form for user id */}
        <input type="number" min ="1" max ="24999" {...userId} autoComplete="new-id" />
      </div>
      <br />
      {/* two buttons one for logout and one for prediction */}
      <input type="button" onClick={handleLogout} value="Logout" />
      <input type="button" onClick={handlePrediction} value="Prediction" />
      <br />
      
      <div className='container'>
        <h1>Your List of Movies</h1>
      </div>
      <div className='repo-container'>
        {/* list being displayed or not based on the state of the app */}
        <ListLoading isLoading={appState.loading} repos={appState.repos} />
      </div>

    </div>
  );
  
}

export default withRouter(Dashboard);