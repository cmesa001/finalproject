//imports
import React, { useState, useEffect } from 'react';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import axios from 'axios';

import { Navigation, Footer, Home, Login, Dashboard, Register } from "./components";

import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import { getToken, removeUserSession, setUserSession } from './Utils/Common';

function App() {
  const [authLoading, setAuthLoading] = useState(true);// state for authentification
 
  useEffect(() => {//function that deals with token 
    const token = getToken();
    if (!token) {
      return;
    }
    axios.get(`http://localhost:4000/verifyToken?token=${token}`).then(response => {//get response from backend to see if the token is correct
      setUserSession(response.data.token, response.data.user);//start user session
      setAuthLoading(false);
    }).catch(_error => {
      removeUserSession();
      setAuthLoading(false);
    });
  }, []);

  if (authLoading && getToken()) {
    return <div className="content">Checking Authentication...</div>//HTML component rendered while waiting for token validation
  }

  return (//returns HTML main component dealing with the accesability for diffrent routes
    <div className="App">
      <Router>
        <Navigation />
        <Switch>
          <Route path="/" exact component={() => <Home />} />
          <PublicRoute path="/login" exact component={() => <Login />} />
          <PublicRoute path="/register" exact component={() => <Register />} />
          <PrivateRoute path="/dashboard" exact component={() => <Dashboard />} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;