//token seed
require('dotenv').config();
//libraries
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const utils = require('./utils');
const model = require("./build-model");
//create app and port
const app = express();
const port = process.env.PORT || 4000;


//connect to user database
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://cmesa001:T2ZI6EDrevS50IQ3@cluster0.fk4ue.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


// enable CORS
app.use(cors());
// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


//middleware that checks if JWT token exists and verifies it if it does exist.
//In all future routes, this helps to know if the request is authenticated or not.
app.use(function (req, res, next) {
// check header or url parameters or post parameters for token
var token = req.headers['authorization'];
if (!token) return next(); //if no token, continue

token = token.replace('Bearer ', '');
jwt.verify(token, process.env.JWT_SECRET, function (err, user) {//verify token
  if (err) {//handle error
    return res.status(401).json({
      error: true,
      message: "Invalid user."
    });
  } else {//asign user
    req.user = user; //set the user to req so other routes can use it
    next();
  }
});
});


// request handlers
app.get('/', (req, res) => {//root path
if (!req.user) return res.status(401).json({ success: false, message: 'Invalid user to access it.' });
res.send('Welcome to your api - ' + req.user.name);
});
app.listen(port, () => {//port asing 
  console.log('Server started on: ' + port);
});

app.post('/users/registerin', function (req, res) {//API call for user register
  //get values from frontend
const name = req.body.name;
const username = req.body.username;
const email = req.body.email;
const password = req.body.password;
var obj = {//create object with user value
  name: name,
  username: username,
  email: email,
  password: password
};

client.connect(err => {//connect to mongo cluster
  if (err) console.log(err);
  var dbo = client.db("mydb").collection("customers");//select database and collection 
  
  dbo.insertOne(obj, function (err, res) {//insert object in colletion
    if (err) throw err;
    if(res)console.log("user registered")
    else res.status(401).json({error:"true",
  message:"error"})
  
  })
});
// generate token
const token = utils.generateToken(obj);
// get basic user details
const userObj = utils.getCleanUser(obj);
// return the token along with user details
return res.json({ user: userObj, token });
});

app.post('/users/signin', function (req, res) {//API call for user sign in
// return 400 status if username/password is not exist
if (!req.body.username || !req.body.password) {
  return res.status(400).json({
    error: true,
    message: "Username or Password required."
  });
}

client.connect(async err => {//connect to mongodb cluster 
  if (err) throw err;
  var dbo = client.db("mydb").collection("customers");//select collection
  await dbo.findOne({ username: req.body.username },  function (err, result) {
    if (err) throw err;
    if (result == null)
      return res.status(401).json({
        error: true,
        message: "no username"
      });
    else {
      const user = result.username;
      const pwd = result.password;

      // return 401 status if the credential do not match.
      if (req.body.username !== user || req.body.password !== pwd) {
        return res.status(402.).send({
          message: "Username or Password is Wrong."
        });
      }

      // generate token
      const token = utils.generateToken(result);
      // get basic user details
      const userObj = utils.getCleanUser(result);
      // return the token along with user details

      return res.json({ user: userObj, token });
    }
  });
});

});

app.post('/prediction',function (req, res){//API call handling prediction 
  const tagsSet = new Set(req.body.tagSet);//get tag set 
  const userId = Object.values(req.body.userId)[0];//get user id
  console.log(userId)
  model(userId,tagsSet).then(prediction =>{//pas variable to model
    const imdb=prediction.final;
    const list=prediction.results;
    return res.json({imdb,list})// return predicted list 
  })
});

// verify the token and return it if it's valid
app.get('/verifyToken', function (req, res) {
// check header or url parameters or post parameters for token
var token = req.body.token || req.query.token;
if (!token) {
  return res.status(400).json({
    error: true,
    message: "Token is required."
  });
}
// check token that was passed by decoding token using secret
jwt.verify(token, process.env.JWT_SECRET, function (err, user) {
  if (err) return res.status(401).json({
    error: true,
    message: "Invalid token."
  });

  // return 401 status if the userId does not match.
  if (user.userId !== userData.userId) {
    return res.status(401).json({
      error: true,
      message: "Invalid user."
    });
  }
  // get basic user details
  var userObj = utils.getCleanUser(userData);
  return res.json({ user: userObj, token });
});
});

client.close();//close mongodb
