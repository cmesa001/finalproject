// generate token using secret from process.env.JWT_SECRET
var jwt = require('jsonwebtoken');

 
// generate token and return it
function generateToken(user) {
  if (!user) return null;
 
  var u = {//user details
    userId: user.userId,
    name: user.name,
    username: user.username,
    isAdmin: user.isAdmin
  };
 
  return jwt.sign(u, process.env.JWT_SECRET, {//returns token
    expiresIn: 60 * 60 * 24 // expires in 24 hours
  });
}
 
// return basic user details
function getCleanUser(user) {
  if (!user) return null;

  return {//returns object with user details
    name: user.name,
    username: user.username,
    email: user.email,
    password: user.password
  };
}
 
module.exports = {//export modules
  generateToken,
  getCleanUser
}