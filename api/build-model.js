
// TensorFlow.js for Node,js
//import libraries
const { all, data, model, profile} = require('@tensorflow/tfjs-node');
const tf = require('@tensorflow/tfjs-node');
const d3 = require("d3");
//file path to datasets folder
const csvUrl =
"file://C:/Users/useyo/Downloads/uni/final project/finalProject/";

const predict = async (userNumber,tgs) => {//function to predict movies based on user id and tagset
    const model = await tf.loadLayersModel(//load the saved model
        csvUrl+'/model-2/model.json'
        );
    const raw = await rawData();//get raw data
    const clean = await Data();//get normalized data
    const final = await getinfo();// get imdb properties 
    rate=Object.values(raw.ratingsData);//raw rating data
    mov=Object.values(raw.movies);//raw movie data
    const users = clean.userProfile;//normalized user profile
    const profile = users[userNumber];//the profile for the user id
    const dd = clean.movieProfile;//normalized movie profiles
    let results=[];
    let infoResults = [];
    for (let movie of mov) {//go through each movie 
        if (tgs.size === 0 || movie.tags.filter(tag => tgs.has(tag)).length >= 1) {//check if tags selected and take only movies that have relevant tags           
          const { stats, tagsData, ratingData } = profile; //variables for user profile
          const movieProfile = dd[movie.movieId].profile;// get normalized movie profile for current movie
          const input = [].concat(stats).concat(tagsData).concat(ratingData).concat(movieProfile);// create input for model prediction
          const result = await model.predict(tf.tensor([input])).data();//predict using input 
          let index = movie.movieId;
          results.push([movie.title,index,result]) // push movie and movie id to list
          results = results.sort( (a, b) => {// sort the movies based on result 
            return b[1] - a[1];
          }).splice(0, 30);//take first 30 movies
        }
      };
    return {results,final};//return recommendation list 
}

const getinfo = async () => {// function that links to datasets in order to get imdb details for each movie
    const rawLinks = tf.data.csv(csvUrl+"ml-latest/links.csv")// inpot links.csv dataset
    const rawIMDB = tf.data.csv(csvUrl+"ml-latest/MovieGenre.csv");//inport Moviegenres.csv dataset
    const links = {}
    const info = {}
    const final = []
    try{
    await rawLinks.forEachAsync(link => {//make list of movie links
        const {movieId,imdbId} = link
        links[movieId]={
            movieId,
            imdbId
        }
    })
    } catch(err){
        console.log(err);
    }
    try{
    await rawIMDB.forEachAsync(imdb =>{//make list of imdb properties
        const {imdbId,Link,Score,Poster} = imdb;
            info[imdbId]={
                imdbId,
                Link,
                Score,
                Poster
            }
        
    })
    }catch(err){
        console.log(err)
    }
    for(const link of Object.entries(links)){//combine lists using the movieId property
        tmp = info[link[1].imdbId]
        if(tmp)
        {
            final[link[0]]={
                movieId:link[1].movieId,
                imdbId:tmp.imdbId,
                Link:tmp.Link,
                Score:tmp.Score,
                Poster:tmp.Poster
            }
        }
        else 
        final[link[0]]={//if imdb properties not available create object with messages instead of propertie
            movieId:link[1].movieId,
            imdbId:"Not available",
            Link:"Not available",
            Score:"Not available",
            Poster:"Not available"
        } 
    }
    return final//return list of movies with properties
}

const rawData = async () => {// function for creating lists from movie dataset and rating data set
    const moviesData = tf.data.csv(
        csvUrl+"ml-latest/movies.csv"
    ).take(30000)// take only 30000 movies to minimize the amount of movies without imdb properties
    const ratings = tf.data.csv(
        csvUrl+"ml-latest/ratings.csv"
    ).take(2431076)// take as many as allowed by computational power
const movies = {};
const tags = [];
try{
await moviesData.forEachAsync( movie => {//create list of movies with genres splitted
    const {movieId,title, genres:movieTags} = movie;
    const tagsSplit = movieTags.split('|');
    tagsSplit.forEach(tag => {
        if ( tags.indexOf(tag)===-1) {//create list with all the tags available
            tags.push(tag);
        }
    });

    movies[movieId] = {
        movieId,
        title,
        tags: tagsSplit
    }
    
});
} catch (error){
    console.log(error);
}
ratingsData = []
try{
await ratings.forEachAsync(e => ratingsData.push(e))//create list of ratings
}catch(error){
    console.log(error);
}

return {//export tags, movie list, and rating list
    tags,
    movies, 
    ratingsData
   }
}

const Data = async () => {
    const raw = await rawData();//get raw data
    //variable initialization
    const movieProfile = {};
    const userProfile = {};
    const trainingData = {xs: [], ys: []};
    const moviesCount = Object.keys((raw).movies).length;
    const increment = 1 / moviesCount;
    rate=Object.values((raw).ratingsData);
    mov=Object.values((raw).movies);
    ttag=(raw).tags

    for (let movie of mov){//for each movie
        const tagsArr =[];
        const {movieId,title} = movie;
        ttag.forEach(tag => {//create movie profile by replacing the genres with an array of 0 and 1 each 1 representing a genre
            tagsArr.push(movie.tags.indexOf(tag) !== -1 ? 1 :0);
        });

        movieProfile[movie.movieId] = {//create list of normalized movie
            movieId,
            title,
            profile: tagsArr
        }
    }
    for (let rat of rate) {//go through all ratings 
        const {userId,movieId,rating} = rat;
        const ratingVal = parseFloat(rating);
        const ratingNormalized = ratingVal / 5;// for each rating normalize value
        
        let user = userProfile[userId];
        
        if (!user) {// if the uer wasn't loked at initialize with following
          user = {
            stats: [ 1, 0 ],
            tagsData: ttag.map( () => 0 ),
            ratingData: d3.range(10).map( () => 0 )
          }
          userProfile[userId] = user;//asign to user profile
        }
        
        //change user stats
        if (user.stats[0] > ratingNormalized) user.stats[0] = ratingNormalized;
        if (user.stats[1] < ratingNormalized) user.stats[1] = ratingNormalized;
        const movie = mov[movieId];
        if (movie) {
          const { tags } = movie;
          tags.forEach( tag => {
            user.tagsData[ttag.indexOf(tag)] += increment;//create score for each tag 
          });
          user.ratingData[ Math.floor(ratingVal * 2) - 1 ] += increment;//create score for each movie
        }
      }
      for (let rat of rate) {//create training variables
        const { userId, movieId} = rat;
        ratingNormalized = rat.rating / 5;
        const user = userProfile[userId];
        const movie = movieProfile[movieId];
        if (movie) {
          const { stats, tagsData, ratingData } = user;
          //training sample
           trainingData.xs.push([].concat(stats).concat(tagsData).concat(ratingData).concat(movie.profile));// concatenate all info for that specific movie
           //lable sample
           trainingData.ys.push(ratingNormalized)
         }
       }
    return {//returned all variables created
        movieProfile,
        userProfile,
        trainingData,
        features: trainingData.xs[0].length,
        trainedModel: false,
        moviesCount: Object.keys(movieProfile).length
    }
}

const modelStructure = async () => {// define structure of the model
    const d = await Data();//get normalized data
    const model = tf.sequential();//declare model type
    const xsLength = d.features;//number of fetures 

    model.add(tf.layers.dense({units: 8, inputShape: [xsLength], activation: 'relu6'}));//first layer config
    model.add(tf.layers.dense({units: 1}));//last layer config 
  
    model.compile({optimizer: 'sgd', loss: 'meanSquaredError', metrics: ['accuracy']});//compilation method

    return model;// return model

}

const trainBatch = async () => {
    const tmp = await Data();// get data
    console.log("start");
    //hyper parameters 
    const batchIndex=0;
    const batchSize=512;
    const epochs = 20;
    const xsLength =52;
    const from = batchIndex*batchSize;
    const to = from +batchSize;

    //training set and label set
    const xs = tf.tensor2d(tmp.trainingData.xs.slice(from, to), [batchSize, xsLength]);   
    const ys = tf.tensor2d(tmp.trainingData.ys.slice(from, to), [batchSize, 1]);

    const model = await modelStructure(); //get model structure

    const history = model.fit(xs, ys, {// fit model using training and label set 
        epochs,
        validationSplit: 0.2
    });
    console.log("end!")
    const savedProgress = model.save('file://model-2');//save the model 
    return savedProgress;
}

module.exports = predict; // export predict function to be used in server.js