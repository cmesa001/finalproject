# finalProject

In order to run the finalProject you will need to: 

You will need to change the path in the build-model.js file in order for the program to be able to find the datasets and the model saved

current path: "file://C:/Users/useyo/Downloads/uni/final project/finalProject/"

change it to: "file:/your/path/to/directory/where/the/project/folder/is/finalProject/"

1.Run server side:
-> go into directory "api"
-> run following command (while in directory) in terminal: "npm run serve"

2.Run React.js app
->open new terminal and go int directory "client/src"
->run following command "npm start App.js"

The database is hosted on AWS servers in Ireland. A cluster was made on Mongodb Atlas.

All the necesary files are present in the repository
